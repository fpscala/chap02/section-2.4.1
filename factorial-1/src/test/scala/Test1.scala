import org.junit.Test
import org.junit.Assert._
import Factorial.factorial1
import Factorial.factorial2

class Test1 {
  @Test def t1(): Unit = {
    assertEquals(120, factorial1(5))
    assertEquals(120, factorial2(5))
  }
}
