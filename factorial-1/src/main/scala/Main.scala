import Factorial.factorial1
import Factorial.factorial2

object Main extends App {
  val n = 5
  val f1 = factorial1(n)
  val f2 = factorial2(n)
  println(f1)
  println(f2)
}
