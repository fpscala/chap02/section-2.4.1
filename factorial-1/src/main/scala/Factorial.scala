object Factorial {
  def factorial1(n: Int): Int =
    if (n == 1) 1 else factorial1(n - 1) * n

  def factorial2(n: Int): Int = {
    def go(n: Int, acc: Int): Int =
      if (n <= 0) acc else go(n - 1, n * acc)
    go(n, 1)
  }
}
